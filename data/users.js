module.exports = [
    {
        id: 1,
        name: 'Daniel',
        email: 'daniel@mail.com',
        password: 'daniel',
        createdAt: Date.now(),
        updatedAt: Date.now(),
    },
    {
        id: 2,
        name: 'Jeanne',
        email: 'jeanne@mail.com',
        password: 'jeanne',
        createdAt: Date.now(),
        updatedAt: Date.now(),
    },
]
